package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter { 
  Rectangle2D box;
  GridDimension gd;
  double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition cp) {
    double cellWidth = (box.getWidth() -(gd.cols() +1)*margin) / gd.cols();
    double cellHeight = (box.getHeight() -(gd.rows() +1)*margin) / gd.rows();

    double CellX = (box.getX() + (cp.col()+1)*margin + cp.col()*cellWidth);
    double CellY = (box.getY() + (cp.row()+1)*margin + cp.row()*cellHeight);

    Rectangle2D cell = new Rectangle2D.Double(CellX, CellY, cellWidth, cellHeight);
    return cell;
  }
}
