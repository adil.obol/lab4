package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;
import java.awt.Color;
import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {

  ColorGrid colorGrid = new ColorGrid(3, 4);
  colorGrid.set(new CellPosition(0,0), Color.RED);
  colorGrid.set(new CellPosition(0,3), Color.BLUE);
  colorGrid.set(new CellPosition(2,0), Color.YELLOW);
  colorGrid.set(new CellPosition(2,3), Color.GREEN);
  }
}
