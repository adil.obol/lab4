package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

public class ColorGrid implements IColorGrid{

  int rows;
  int cols;
    HashMap<CellPosition, Color> verdier = new HashMap<>();

  CellPosition pos = null;

  public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;

    for (int col = 0; col < cols; col++) {
      for (int row = 0; row < rows; row++) {
        pos = new CellPosition(row, col);
        verdier.put(pos, null);
      }
    }
  }

  public int rows() {
    return this.rows;
  }

  public int cols() {
    return this.cols;
  }

  public void set(CellPosition pos, Color color) {
    if (verdier.containsKey(pos)) {
      verdier.put(pos, color);
    }
    else {
      throw new IndexOutOfBoundsException("Position is out of bounds");
    }
  }

  public Color get(CellPosition pos) {
    if (verdier.containsKey(pos)) {
      return verdier.get(pos);
    }
    else {
      throw new IndexOutOfBoundsException("Position is out of bounds");
    }
  }

  public List<CellColor> getCells() {
    ArrayList<CellColor> cellColors = new ArrayList<>();
    for (HashMap.Entry<CellPosition, Color> entry : verdier.entrySet()) {
        CellColor cellColor = new CellColor(entry.getKey(), entry.getValue());
        cellColors.add(cellColor);
    }
    return cellColors;
}

}

